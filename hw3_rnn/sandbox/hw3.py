import tensorflow as tf
from tensorflow.python.client import device_lib
import razdel
import collections
import tqdm
from sklearn.model_selection import train_test_split
import numpy as np
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Embedding, GRU, LSTM, Lambda
from tensorflow.keras.callbacks import ModelCheckpoint
from keras_tqdm import TQDMNotebookCallback
import random


print(device_lib.list_local_devices())
print('Default GPU Device: {}'.format(tf.test.gpu_device_name()))

with open('./puskin.txt/output.txt', encoding='utf8') as f:
    data1 = f.read().splitlines()
    
data = [d.replace('\t','').replace('"','').replace('[','').replace(']','').replace(r'\xa0—','') for d in data1 if d != '' and d != '* * *']
data = data[7:]

SEQUENCE_START = '<START>'
SEQUENCE_END = '<END>'


def tokenize(text):
    return [SEQUENCE_START] + [t.text for t in razdel.tokenize(text.lower())] + [SEQUENCE_END]



PAD_INDEX = 0
UNKNOWN_INDEX = 1


token_counts = collections.Counter(token for item in tqdm.tqdm_notebook(data) for token in tokenize(item))

vocabulary = {'PAD': PAD_INDEX, 'UNKNOWN': UNKNOWN_INDEX}
for token, count in token_counts.items():
    if count > 1:
        vocabulary[token] = len(vocabulary)
        
tokenized = [tokenize(text) for text in data]

MAX_TEXT_LENGTH = max(map(len, tokenized))

samples = []
targets = []
for tokens in tqdm.tqdm_notebook(tokenized):
    indices = [vocabulary.get(token, UNKNOWN_INDEX) for token in tokens]
    for i in range(1, len(tokens)):
        targets.append(indices[i])
        samples.append(indices[:i])


X_train, X_test, y_train, y_test = train_test_split(np.array(samples), np.array(targets), test_size=0.1)


X_train = pad_sequences(X_train, MAX_TEXT_LENGTH)
X_test = pad_sequences(X_test, MAX_TEXT_LENGTH)





    


token_by_index = {index: token for token, index in vocabulary.items()}

SEQUENCE_START_INDEX = vocabulary[SEQUENCE_START]
SEQUENCE_END_INDEX = vocabulary[SEQUENCE_END]


def generate():
    indices = [vocabulary[SEQUENCE_START]]
    while True:
        sequence = pad_sequences([indices], MAX_TEXT_LENGTH)
        predictions = model.predict(sequence)[0]
        if predictions.argmax() == SEQUENCE_END_INDEX:
            return ' '.join(token_by_index[index] for index in indices[1:])
        
        seed = random.random()
        
        total = 0
        for i, probability in enumerate(predictions):
            total += probability
            if seed < total and i not in [UNKNOWN_INDEX, SEQUENCE_START_INDEX, SEQUENCE_END_INDEX] and i != indices[-1]:
                indices.append(i)
                break


def fit(epochs=1):
    checkpoint = ModelCheckpoint('advices-{epoch:02d}-{loss:.4f}', monitor='loss', verbose=1, save_best_only=True, mode='min')
    model.fit(
        X_train, 
        y_train, 
        epochs=epochs, 
        verbose=0,
        batch_size=128, 
        validation_data=(X_test, y_test),
       #callbacks=[checkpoint, TQDMNotebookCallback()],
    )
    
EMBEDDING_DIM = 100
VOCABULARY_SIZE = len(vocabulary)

temperature = 0.6
model = Sequential()
model.add(Embedding(input_dim=VOCABULARY_SIZE, output_dim=EMBEDDING_DIM, input_length=MAX_TEXT_LENGTH))
model.add(LSTM(512, input_shape=(MAX_TEXT_LENGTH, EMBEDDING_DIM), return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(1024, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(512))
model.add(Dropout(0.7))
model.add(Lambda(lambda x: x / temperature))
model.add(Dense(VOCABULARY_SIZE, activation='softmax'))

model.compile(loss='sparse_categorical_crossentropy', optimizer='adam')
model.summary()

fit(epochs=20)
for _ in range(4):
    print(generate())
    print('-'*10)

print('saving model')
model.save('./')
