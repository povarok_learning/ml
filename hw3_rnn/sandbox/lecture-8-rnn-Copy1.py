print('sdf')
import tensorflow as tf
from tensorflow.python.client import device_lib
import razdel
from string import punctuation
import gensim.parsing.preprocessing as gsp
import re
import json
import razdel
from string import punctuation
import gensim.parsing.preprocessing as gsp
import re
import collections
import tqdm
from sklearn.model_selection import train_test_split
import numpy as np
from tensorflow.keras.preprocessing.sequence import pad_sequences


with open('messages.json') as json_file:
    data = json.load(json_file)


filters = [
    gsp.strip_punctuation,
    gsp.strip_numeric,
    gsp.strip_multiple_whitespaces,
    gsp.strip_tags,
]

def clean_urls(s):
    for u in re.findall(r'(https?://\S+)', s):
        s = s.replace(u, '')
    return s

def clean(t):
    t = t.lower()
    t = clean_urls(t)
    t = re.sub('[A-Za-z]', '', t)
    t = t.replace('–', '')
    t = t.replace('—', '')
#     t = t.replace(' ', '')
    t = t.replace('«', '')
    t = t.replace('»', '')
    t = t.replace('»', '')
    t = t.replace('………………………………', '')
    t = t.replace('…………………………', '')
    
    for f in filters:
        t = f(t)
    t = t.strip()
    return t

texts = []
for c in data['conversations']:
    if c['properties']['lastimreceivedtime'] is None:
        continue
    t = int(c['properties']['lastimreceivedtime'][:4])
    if t >= 2017:
        if 'threadProperties' in c:
            if c['threadProperties'] and 'membercount' in c['threadProperties']:
                for t in c['MessageList']:
                    texts.append(t['content'])

texts1 = [t for t in texts if len(t)]
texts1 = [clean(t) for t in texts1]
texts1 = [t for t in texts1 if len(t)]
data = texts1


SEQUENCE_START = '<START>'
SEQUENCE_END = '<END>'


def tokenize(text):
    return [SEQUENCE_START] + list(filter(lambda x:len(x)>0,[t.text for t in razdel.tokenize(text.lower())])) + [SEQUENCE_END]

data = [clean(row) for row in data]
data = [d for d in data if len(d)]


PAD_INDEX = 0
UNKNOWN_INDEX = 1

token_counts = collections.Counter(token for item in data for token in tokenize(item))
vocabulary = {'PAD': PAD_INDEX, 'UNKNOWN': UNKNOWN_INDEX}
for token, count in token_counts.items():
    if count > 1:
        vocabulary[token] = len(vocabulary)

print(len(vocabulary))

tokenized_def = [tokenize(text) for text in data]

tokenized = tokenized_def
tokenized = list(filter(lambda x: len(x)>2 and len(x) <= 15, tokenized))
MAX_TEXT_LENGTH = max(map(len, tokenized))
print(MAX_TEXT_LENGTH)
len(tokenized)

samples = []
targets = []
for tokens in tqdm.tqdm(tokenized):
    indices = [vocabulary.get(token, UNKNOWN_INDEX) for token in tokens]
    for i in range(1, len(tokens)):
        targets.append(indices[i])
        samples.append(indices[:i])


X_train, X_test, y_train, y_test = train_test_split(np.array(samples), np.array(targets), test_size=0.1)
X_train = pad_sequences(X_train, MAX_TEXT_LENGTH)
X_test = pad_sequences(X_test, MAX_TEXT_LENGTH)

print(X_train.shape, X_test.shape)

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Embedding, GRU, LSTM

EMBEDDING_DIM = 300
VOCABULARY_SIZE = len(vocabulary)

#model = Sequential()
#model.add(Embedding(input_dim=VOCABULARY_SIZE, output_dim=EMBEDDING_DIM, input_length=MAX_TEXT_LENGTH))
#model.add(GRU(256, input_shape=(MAX_TEXT_LENGTH, EMBEDDING_DIM)))
#model.add(Dropout(0.2))
#model.add(Dense(VOCABULARY_SIZE, activation='softmax'))

model = Sequential()
model.add(Embedding(input_dim=VOCABULARY_SIZE, output_dim=EMBEDDING_DIM, input_length=MAX_TEXT_LENGTH))
model.add(LSTM(512, input_shape=(MAX_TEXT_LENGTH, EMBEDDING_DIM), return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(1024, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(512))
model.add(Dropout(0.2))
model.add(Dense(VOCABULARY_SIZE, activation='softmax'))

model.compile(loss='sparse_categorical_crossentropy', optimizer='adam')
model.summary()

#from tensorflow.keras.callbacks import ModelCheckpoint
#from keras_tqdm import TQDMNotebookCallback


def fit(epochs=1):
#    checkpoint = ModelCheckpoint('advices-{epoch:02d}-{loss:.4f}', monitor='loss', verbose=1, save_best_only=True, mode='min')
    model.fit(
        X_train, 
        y_train, 
        epochs=epochs, 
        verbose=1,
        batch_size=128, 
        validation_data=(X_test, y_test),
       #callbacks=[checkpoint, TQDMNotebookCallback()],
    )


# ## Генерация текста

# In[30]:


import numpy as np
import random

token_by_index = {index: token for token, index in vocabulary.items()}

SEQUENCE_START_INDEX = vocabulary[SEQUENCE_START]
SEQUENCE_END_INDEX = vocabulary[SEQUENCE_END]


def generate():
    indices = [vocabulary[SEQUENCE_START]]
    while True:
        sequence = pad_sequences([indices], MAX_TEXT_LENGTH)
        predictions = model.predict(sequence)[0]
        if predictions.argmax() == SEQUENCE_END_INDEX:
            return ' '.join(token_by_index[index] for index in indices[1:])
        
        seed = random.random()
        
        total = 0
        for i, probability in enumerate(predictions):
            total += probability
            if seed < total and i not in [UNKNOWN_INDEX, SEQUENCE_START_INDEX, SEQUENCE_END_INDEX] and i != indices[-1]:
                indices.append(i)
                break

fit(epochs=1)

for _ in range(100):
    print(generate())
    print('-'*10)
import datetime
model.save(f'model_{datetime.datetime.now().strftime("%m_%d_%Y_%H_%M_%S")}')





